# Evaluation of BLUE algorithm using ns-3

## Description
There is a reignition of Active Queue Management (AQM) in the field of research due to the bufferbloat problem resulting in high queuing delay. The Internet Engineering Task Force (IETF) has taken into consideration the implementation of AQM techniques for the sake of increasing network traffic resulting in high packet loss rates. This remarkably increasing demand for the internet and services results in congestion, where buffer management plays an important role. Before the network algorithms are deployed on the Internet, there is a need to simulate the network algorithms in order to gain an accurate and deep understanding of the algorithms.
ns-3 is among one of the most powerful and widely used network simulators. The Linux-like traffic control subsystem makes ns-3 highly suitable and reliable for examining the performance of queuing disciplines. A primitive active queue management algorithm, namely BLUE, is upgraded, validated, and evaluated. Using simulation and controlled experiments BLUE has performed remarkably better in the network. 
We did the upgradation and validation of BLUE queueing discipline in ns-3.

## Installation
Download and install ns-3-dev: https://gitlab.com/nsnam/ns-3-dev

## Usage
BLUE algorithm was earlier implemented as queuing discipline in the traffic control subsystem of ns-3. But the source code available for ns-3 is outdated and needs an upgrade.
In this repository, we present the revision of implementation of BLUE algorithm to the latest version of ns-3. We believe that the revision of BLUE in ns-3 would help to extend the functionality to meet the demands of modern Internet traffic.

## Test
The updated src/traffic-control/test/blue-queue-disc-test-suite.cc is used for validating the BLUE queue management algorithm in ns-3. 

For remaining documentation see the src/traffic-control/doc/blue.rst


